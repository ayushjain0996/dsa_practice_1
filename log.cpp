#include<iostream>
using namespace std;

int main(){
    int n;
    int base;
    cin>>n>>base;
    int log=0;
    int fac = 1;
    while(fac<=n){
        fac = fac * base;
        log++;
    }
    cout<<log-1;
    return 0;
}
