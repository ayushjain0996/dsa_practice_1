#include<iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int dec;
        cin>>dec;
        int count = 0;
        while(dec>0){
            if(dec%2!=0){
                count++;
            }
            dec/=2;
        }
        cout<<count<<endl;
    }
    return 0;
}
