#include <iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int count0 = 0;
    int count1 = 0;
    for(int i=0; i<n; i++){
        int x;
        cin>>x;
        if(x == 1){
            count1++;
        }
        else{
            count0++;
        }
    }
    for(int i=0; i<count0; i++){
        cout<<"0 ";
    }
    for(int i=0; i<count1; i++){
        cout<<"1 ";
    }

    return 0;
}