#include<iostream>
using namespace std;
int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        int cost = 0;
        for(int i=0; i<n; i++){
            for(int j=i; j<n; j++){
                if(arr[j]<arr[i]){
                    cost++;
                }
            }
        }
        cout<<cost<<endl;
    }
    return 0;
}