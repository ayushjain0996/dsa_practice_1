#include<iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int cost[1000];
    for(int i=0; i<n; i++){
        cin>>cost[i];
    }
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int money, num;
        int actNum = 0;
        cin>>money>>num;
        for(int i=0; i<n; i++){
            if(money%cost[i]==0){
                actNum++;
            }
        }
        if(actNum == num){
            cout<<"Yes"<<endl;
        }
        else{
            cout<<"No"<<endl;
        }
    }
    return 0;
}