#include<iostream>
using namespace std;
int main(){
    int m,n;
    cin>>m>>n;
    int arr[30][30];
    for(int i=0; i<m; i++){
        for(int j=0; j<n; j++){
            cin>>arr[i][j];
        }
    }
    int num;
    cin>>num;
    int i=0, j=n-1;
    while((i>=0 && i<m) && (j>=0 && j<n)){
        if(arr[i][j]==num){
            cout<<true<<endl;
            return 0;
        }
        else if(arr[i][j]<num){
            i++;
        }
        else{
            j--;
        }
    }
    cout<<false<<endl;
    return 0;
}