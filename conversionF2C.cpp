#include<iostream>

using namespace std;

int main(){
    int min;
    int max;
    int step;
    int cel = 0;
    cin>>min>>max>>step;
    while(min<=max){
        cel = (5 * (min - 32))/9;
        cout<<min<<" "<<cel<<endl;
        min += step;
    }
    return 0;
}
