#include<iostream>
using namespace std;

int  main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int sum;
    int subsum;
    cin>>sum;
    
    for(int i=0; i<n; i++){
        int j=i;
        subsum = 0;
        while(j<n && subsum<sum){
            subsum += arr[j];
            j++;
        }
        if(subsum==sum){
            cout<<i<<" "<<(j-1);
            return 0;
        }
    }
    cout<<"None"<<endl;
    return 0;
}