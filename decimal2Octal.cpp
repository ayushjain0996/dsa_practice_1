#include<iostream>

using namespace std;

int main(){
    int dec;
    cin>>dec;
    int oct = 0;
    int fac = 1;
    while(dec>0){
        oct = oct + (dec%8) * fac;
        fac = fac * 10;
        dec = dec/8;
    }
    cout<<oct;
    return 0;
}
