#include<iostream>
using namespace std;

int multiply(int arr[], int x, int len){
    int carry = 0;
    for(int i=0; i<len; i++){
        int product = arr[i] * x + carry;
        arr[i] = product % 10;
        carry = product / 10;
    }
    while(carry>0){
        arr[len++] = carry%10;
        carry = carry / 10;
    }
    return len;
}
int main(){
    int n;
    cin>>n;
    int arr[113500] = {};
    arr[0] = 1;
    int len = 1;
    for(int i=2; i<=n; i++){
        len = multiply(arr, i, len);
    }
    for(int i=len-1; i>=0; i--){
        cout<<arr[i];
    }
    cout<<endl;    
    return 0;
}