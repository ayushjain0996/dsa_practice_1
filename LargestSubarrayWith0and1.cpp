#include<iostream>
using namespace std;
int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        int count0 = 0, count1 = 0;
        int len = 0, maxlen = 0;
        int pos1 = 0, pos2 = 0;
        for(int i=0; i<n; i++){
            len = 0;
            count0 = 0;
            count1 = 0;
            for(int j=i; j<n; j++){
                if(arr[j]){
                    count1++;
                }
                else{
                    count0++;
                }
                len++;
                if(count0 == count1 && len>maxlen){
                    pos1 = i;
                    pos2 = j;
                    maxlen = len;
                }
            }
        }
        if(maxlen==0){
            cout<<"None"<<endl;
        }
        else{
            cout<<pos1<<" "<<pos2<<endl;
        }
    }
    return 0;
}