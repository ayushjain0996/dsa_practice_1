#include<iostream>
using namespace std;
int main(){
    long int n;
    cin>>n;
    long int fac = 1;
    long int rev = 0;
    while(n>0){
        if((n%10)>=5){
            rev = rev + fac * (9 - n%10);
        }
        else{
            rev = rev + fac * (n%10);
        }
        fac = fac * 10;
        n = n/10;
    }
    fac /= 10;
    if(rev<fac){
        rev+=fac*9;
    }
    cout<<rev;
    return 0;
}
