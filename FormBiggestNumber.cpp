#include<iostream>
using namespace std;
//Selection Sort
//Use standard Selection Sort. Personalize comparision.

bool compare(long int x, long int y){
    //return true for a > b
    long int rx=1, ry=1;
    long int xcopy = x, ycopy = y;
    while(xcopy>0){
        rx *= 10;
        xcopy = xcopy/10;
    }
    while(ycopy>0){
        ry *= 10;
        ycopy = ycopy/10;
    }
    long int xy = x*ry + y;
    long int yx = y*rx + x;
    if(xy>yx){
        return false;
    }
    return true;
}

int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        long int arr[100];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        for(int i=0; i<n; i++){
            for(int j=0; j<n-i-1; j++){
                //comparision
                if(compare(arr[j], arr[j+1])){
                    //swap
                    long int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
        for(int i=0; i<n; i++){
            cout<<arr[i];
        }
        cout<<endl;
    }
    return 0;
}