#include<iostream>

using namespace std;

int main(){
    int n;
    cin>>n;
    for(int i=1;i<=n;i++){
        for(int j=i; j<=(n-1); j++){
            cout<<"\t";
        }
        int k=i;
        for(int j=i; j>0; j--){
            cout<<k++<<"\t";
        }
        k--;
        for(int j=1; j<i; j++){
            cout<<--k<<"\t";
        }
        cout<<endl;
    }
    return 0;
}
