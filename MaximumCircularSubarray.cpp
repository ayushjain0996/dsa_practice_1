//Find minimum value of subarray sum
//Subtract that from total sum
#include<iostream>
using namespace std;
int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int arr[1000];
        int totalSum = 0;
        for(int i=0; i<n; i++){
            cin>>arr[i];
            totalSum += arr[i];
        }
        int currSum = 0, minSum = 0;
        for(int i=0; i<n; i++){
            currSum = currSum + arr[i];
            if(currSum>0){
                currSum = 0;
            }
            else if(currSum<minSum){
                minSum = currSum;
            }
        }
        cout<<totalSum - minSum<<endl;
    }
    return 0;
}