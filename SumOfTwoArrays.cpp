#include<iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    int arr1[1000];
    for(int i=0; i<n; i++){
        cin>>arr1[i];
    }
    int m;
    cin>>m;
    int arr2[1000];
    for(int i=0; i<m; i++){
        cin>>arr2[i];
    }
    int arrsum[1001] = {};
    int i=n-1, j=m-1, k=0;
    int sum = 0;
    int carry = 0;
    while(i>=0 && j>=0){
        sum = arr1[i--] + arr2[j--] + carry;
        arrsum[k++] = sum % 10;
        carry = sum/10;
    }
    while(i>=0){
        sum = arr1[i--] + carry;
        arrsum[k++] = sum % 10;
        carry = sum/10;
    }
    while(j>=0){
        sum = arr2[j--] + carry;
        arrsum[k++] = sum % 10;
        carry = sum/10;
    }
    if(carry != 0){
        arrsum[k++] = carry; 
    }
    for(int p = k-1; p>=0; p--){
        cout<<arrsum[p]<<", ";
    }
    cout<<"END"<<endl;
    return 0;
}