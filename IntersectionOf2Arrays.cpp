#include<iostream>
using namespace std;

void input(int arr[], int n){
    for(int i=0; i<n;i++){
        cin>>arr[i];
    }
}
void output(int arr[], int n){
    cout<<"[";
    for(int i=0; i<n; i++){
        if(i==0){
            cout<<arr[0];
        }
        else{
            cout<<", "<<arr[i];
        }
    }
    cout<<"]"<<endl;
}

void sort(int arr[], int n){
    for(int i=0; i<n; i++){
        for(int j=0; j<n-i-1; j++){
            if(arr[j]>arr[j+1]){
                int temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
}

int main(){
    int n;
    cin>>n;
    int arr1[1000], arr2[1000];
    input(arr1, n);
    input(arr2, n);
    sort(arr1, n);
    sort(arr2, n);
    int p1 = 0, p2 = 0;
    int inter[1000];
    int len = 0;
    while(p1<n && p2<n){
        if(arr1[p1] == arr2[p2]){
            inter[len++] = arr1[p1];
            p1++;
            p2++;
        }
        else if(arr1[p1]>arr2[p2]){
            p2++;
        }
        else{
            p1++;
        }
    }
    output(inter, len);    
    return 0;
}