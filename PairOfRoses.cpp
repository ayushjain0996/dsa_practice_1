#include<iostream>
using namespace std;

int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int arr[10000] = {};
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        int money;
        cin>>money;
        
        for(int i=0; i<n; i++){
            int minpos = i;
            int minval = arr[i];
            for(int j=i; j<n; j++){
                if(minval>arr[j]){
                    minpos = j;
                    minval = arr[j];
                }
            }
            arr[minpos] = arr[i];
            arr[i] = minval;
        }
        int start = 0, end = n-1;
        int rose1=0, rose2=0;
        while(start<end){
            int sum = arr[start] + arr[end];
            if(sum == money){
                rose1 = arr[start];
                rose2 = arr[end];
                start++;
                end--;
            }
            else if(arr[end]>=money){
                end--;
            }
            else if(sum>money){
                if(arr[start+1]+arr[end]==money){
                    start++;
                }
                else{
                    end--;
                }
            }
            else{
                start++;
            }
        }

        cout<<"Deepak should buy roses whose prices are "<<rose1<<" and "<<rose2<<".\n";
    }    
    return 0;
}