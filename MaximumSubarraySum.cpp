#include<iostream>
using namespace std;

int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int arr[100000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        int maxsum = 0;
        int curr = 0;
        for(int i=0; i<n; i++){
            curr = curr + arr[i];
            if(curr<0){
                curr = 0;
            }
            if(curr>maxsum){
                maxsum = curr;
            }
        }
        cout<<maxsum<<endl;
    }
    return 0;
}