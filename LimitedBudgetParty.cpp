#include<iostream>
using namespace std;

bool check(int arr[], int n, int X){
    int sum = 0;
    for(int i=0; i<n; i++){
        sum = 0;
        for(int j=i; j<n && sum<=X ; j++){
            sum += arr[j];
            if(sum == X){
                return true;
            }
        }
    }
    return false;
}

int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int n;
        cin>>n;
        int X;
        cin>>X;
        int arr[1000];
        for(int i=0; i<n; i++){
            cin>>arr[i];
        }
        if(check(arr, n, X)){
            cout<<"YES"<<endl;
        }
        else{
            cout<<"NO"<<endl;
        }
    }    
    return 0;
}