#include<iostream>
using namespace std;
bool compare(char str1[], char str2[]){
    //tells if str1 comes after str2
    int i=0;
    while(str1[i] && str2[i]){
        if(str1[i] > str2[i]){
            return true;
        }
        else if(str1[i] < str2[i]){
            return false;
        }
        i++;
    }
    if(str2[i]!=0){
        return true;
    }
    else{
        return false;
    }
}
void swap(char arr1[], char arr2[]){
    char temp[1000] = {};
    int i=0;
    while(arr1[i]){
        temp[i] = arr1[i];
        i++;
    }
    i=0;
    while(arr2[i]){
        arr1[i] = arr2[i];
        i++;
    }
    arr1[i] = 0;
    i=0;
    while(temp[i]){
        arr2[i] = temp[i];
        i++;
    }
    arr2[i] = 0;
    return;
}

int main(){
    char str[1000][1000];
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>str[i];
    }
    //sort
    for(int i=0; i<n; i++){
        for(int j=0; j<n-i-1; j++){
            //compare
            if(compare(str[j], str[j+1])){
                //swap
                swap(str[j], str[j+1]);
            }
        }
    }
    for(int i=0; i<n; i++){
        cout<<str[i]<<endl;
    }
    return 0;
}