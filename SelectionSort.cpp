#include<iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    for(int i=0; i<n; i++){
        int minpos = i;
        int minval = arr[i];
        for(int j=i; j<n; j++){
            if(arr[j]<=minval){
                minpos = j;
                minval = arr[j];
            }
        }
        arr[minpos] = arr[i];
        arr[i] = minval;
    }
    for(int i=0; i<n; i++){
        cout<<arr[i]<<endl;
    }
    return 0;
}
