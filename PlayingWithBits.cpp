#include<iostream>
using namespace std;

int setBits(int x){
    int setBits = 0;
    while(x>0){
        if(x%2==1){
            setBits++;
        }
        x = x/2;
    }
    return setBits;
}

int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){
        int a, b;
        cin>>a>>b;
        int setSum = 0;
        for(int i=a; i<=b; i++){
            setSum = setSum + setBits(i);
        }
        cout<<setSum<<endl;
    }
    return 0;
}