#include<iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    int arr[100000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int bits[32] = {};
    for(int i=0; i<n; i++){
        int j = 0;
        while (arr[i])
        {
            bits[j] = bits[j] + arr[i]%2;
            arr[i] = arr[i]/2;
            j++;
        }
    }
    int x = 0;
    int r = 1;
    for(int i=0; i<32; i++){
        x = x + (bits[i]%3)*r;
        r = r * 2;
    }
    cout<<x<<endl;
    return 0;
}