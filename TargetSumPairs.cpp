#include<iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int sum;
    cin>>sum;
    for(int i=0; i<n; i++){
        int minpos = i;
        int minval = arr[i];
        for(int j=i; j<n; j++){
            if(minval>arr[j]){
                minpos = j;
                minval = arr[j];
            }
        }
        arr[minpos] = arr[i];
        arr[i] = minval;
    }
    for(int i=0; i<n; i++){
        for(int j=i+1; j<n; j++){
            if(arr[i] + arr[j] == sum){
                cout<<arr[i]<<" and "<<arr[j]<<endl;
            }
        }
    }
    return 0;
}