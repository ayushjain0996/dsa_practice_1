#include<iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int sum;
    cin>>sum;
    //sort
    for(int i=0; i<n; i++){
        int minval = arr[i];
        int minpos = i;
        for(int j=i; j<n; j++){
            if(minval>arr[j]){
                minval = arr[j];
                minpos = j;
            }
        }
        arr[minpos] = arr[i];
        arr[i] = minval;
    }
    for(int i=0; i<n; i++){
        for(int j=i+1; j<n; j++){
            for(int k=j+1; k<n; k++){
                if(arr[i] + arr[j] + arr[k] == sum){
                    cout<<arr[i]<<", "<<arr[j]<<" and "<<arr[k]<<endl;
                }
            }
        }
    }
    return 0;
}