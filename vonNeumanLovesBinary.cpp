#include<iostream>
using namespace std;
int main(){
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int bin;
        cin>>bin;
        int dec = 0;
        int fac = 1;
        while(bin>0){
            dec = dec + fac * (bin%10);
            fac = fac * 2;
            bin = bin/10;
        }
        cout<<dec<<endl;
    }
    return 0;
}
