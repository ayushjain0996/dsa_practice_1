#include<iostream>
using namespace std;
int main(){
    char str[1000];
    cin.getline(str, 1000);
    int freq[26] = {};
    int i=0;
    while(str[i]){
        freq[(str[i++]-'a')]++;
    }
    int maxi = 0, maxval = 0;
    for(int i=0; i<26; i++){
        if(maxval<freq[i]){
            maxi = i;
            maxval = freq[i];
        }
    }
    cout<<(char)('a' + maxi)<<endl;
    return 0;
}