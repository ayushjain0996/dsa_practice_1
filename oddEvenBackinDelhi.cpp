#include<iostream>
using namespace std;

int main(){
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        int num;
        cin>>num;
        int even=0;
        int odd=0;
        while(num>0){
            if(num%2==0){
                even+=num%10;
            }
            else{
               odd += num%10;
            }
            num = num/10;
        }
        if(odd%3==0 || even%4==0){
            cout<<"Yes\n";
        }
        else{
            cout<<"No\n";
        }
    }
    return 0;
}
